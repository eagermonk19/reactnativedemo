/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  ActivityIndicator,
  Button,
  TextInput,
  Keyboard,
  TouchableOpacity,
  Alert,
} from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      shoppingListData: [],
      id: '',
      item_name: '',
      description: '',
      unit_price: '',
      quantity: '',
    };
    this.handleIdChange = this.handleIdChange.bind(this);
    this.handleItemNameChange = this.handleItemNameChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleQuantityChange = this.handleQuantityChange.bind(this);
  }

  componentDidMount() {
    this.getData();
  }
  handleIdChange(id) {
    this.setState({id});
  }

  handleItemNameChange(item_name) {
    this.setState({item_name});
  }
  handleDescriptionChange(description) {
    this.setState({description});
  }
  handleQuantityChange(quantity) {
    this.setState({quantity});
  }
  getData = async () => {
    await fetch('http://192.168.43.77:3333/list', {
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(responseData => {
        this.setState({
          shoppingListData: responseData,
        });
      })
      .catch(error => {
        console.error(error);
      });
  };
  deleteItem = async id => {
    await fetch('http://192.168.43.77:3333/list/' + id, {
      method: 'delete',
    })
      .then(response => {
        this.getData();
      })
      .then(response => {
        Alert.alert('Item deleted');
      })
      .catch(error => {
        console.log(error);
      });
  };
  addItem = async () => {
    await fetch('http://192.168.43.77:3333/list', {
      method: 'POST',
      body: JSON.stringify({
        id: this.state.id,
        item_name: this.state.item_name,
        description: this.state.description,
        unit_price: this.state.unit_price,
        quantity: this.state.quantity,
      }),
    })
      .then(response => {
        Alert.alert('Item Added!');
      })
      .catch(error => {
        console.error(error);
      });
      this.getData();
  };
  render() {
    return this.state.shoppingListData.length > 0 ? (
      <>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.textInput}
            placeholder="ID"
            maxLength={20}
            value={this.state.id}
            onChangeText={this.handleIdChange}
          />
          <TextInput
            style={styles.textInput}
            placeholder="Item Name"
            maxLength={20}
            value={this.state.item_name}
            onChangeText={this.handleItemNameChange}
          />
          <TextInput
            style={styles.textInput}
            placeholder="Description"
            maxLength={20}
            value={this.state.description}
            onChangeText={this.handleDescriptionChange}
          />
          <TextInput
            style={styles.textInput}
            placeholder="Unit Quantity"
            maxLength={20}
            value={this.state.quantity}
            onChangeText={this.handleQuantityChange}
          />

          <TouchableOpacity style={styles.saveButton} onPress={this.addItem}>
            <Text style={styles.saveButtonText}>Save</Text>
          </TouchableOpacity>
          <FlatList
          style={styles.Flatlist}
          data={this.state.shoppingListData}
            ItemSeparatorComponent={this.FlatListItemSeparator}
            renderItem={({item}) => (
              <View style={styles.Lista}>
                <Text style={styles.Text}>{item.item_name}</Text>
                <Button
                  style={styles.button}
                  title="Delete"
                  onPress={this.deleteItem.bind(this, item.id)}></Button>
              </View>
            )}
            keyExtractor={(item, index) => index}
          /> 
        </View>
      </>
    ) : (
      <View>
        <Text>Loading...</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  button: {
    width: '20%',
    paddingLeft: '20px',
    justifyContent: 'center',
    alignItems: 'center',
  },
  Lista: {
    flexDirection: 'row',
    marginTop: 20,
  },
  Flatlist: {
    width: '100%',
    height: 300,
  },
  ListContainer: {
    flexDirection: 'row',
    width: '100%',
    height: 40,
  },
  Text: {
    marginLeft: '10%',
    width: '70%',
    justifyContent: 'center',
  },
  InputBox: {
    width: '70%',
    justifyContent: 'center',
  },
  InputText: {
    paddingLeft: 20,
    width: '100%',
    height: 50,
    fontSize: 20,
  },
  inputContainer: {
    paddingTop: 15,
  },
  textInput: {
    borderColor: '#CCCCCC',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    height: 50,
    fontSize: 25,
    paddingLeft: 20,
    paddingRight: 20,
  },
  saveButton: {
    borderWidth: 1,
    borderColor: '#007BFF',
    backgroundColor: '#007BFF',
    padding: 15,
    margin: 5,
  },
  saveButtonText: {
    color: '#FFFFFF',
    fontSize: 20,
    textAlign: 'center',
  },
});
